package com.swapnil.citycomfortpartner.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.swapnil.citycomfortpartner.Adpter.AllPaulerProductDataAdpter;
import com.swapnil.citycomfortpartner.Adpter.SubServicesFilterAdpter;
import com.swapnil.citycomfortpartner.Model.Request.PaurlerAllResponse;
import com.swapnil.citycomfortpartner.Model.Response.OfferData;

import com.swapnil.citycomfortpartner.Model.Response.ProductResult;
import com.swapnil.citycomfortpartner.R;
import com.swapnil.citycomfortpartner.Services.Preferences;
import com.swapnil.citycomfortpartner.Services.RetrofitClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FinalOfferFilter extends Fragment {
    Context context;
    ImageButton filter;
    private static final String TAG1 = "MyCustomDialog";

    List<String> subid = new ArrayList<>();
    String exicuteid = Preferences.getUserId();
    RecyclerView recycler_service_sub;
    SubServicesFilterAdpter subServicesAdpter;
//    ArrayList<SubServiceResponse> subServiceResponseArrayList;
    String serviceid;
    String offer_id = "";
    int REQUEST_CODE = 1;
    //copy from fragment
    List<OfferData> offerData;
    List<ProductResult> productResults;
    AllPaulerProductDataAdpter allPaulerProductDataAdpter;


    public FinalOfferFilter() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_final_offer_filter, container, false);

        init(view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        serviceid = Preferences.getServiceId();

        Intent intent = getActivity().getIntent();
//        String s = intent.getStringExtra("sub_id");
//        subid.add(s);
//        if (getArguments() != null) {
//            String off = "off";
//            if (getArguments().getString("off").equals(off)) {
//                String s = getArguments().getString("sub_id");
//                subid.add(s);
//
//
//            }

//            else {
////                Bundle args = getArguments().getSerializable("swap");
//                ArrayList<String> object = (ArrayList<String>) getArguments().getSerializable("swa");
//                Log.v("After Remove", ":: " + object.toString());
//                subid = object;
////                other_product();
//
//            }

//        }

    }

    void init(View view) {
        context = getContext();

//        filter = view.findViewById(R.id.filtersort);
//        filter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Log.d(TAG1, "onClick: opening dialog.");
////
////                Filter dialog = new Filter();
////                dialog.show(getFragmentManager(), "MyCustomDialog");
//
//                Intent intent = new Intent(context, FilterFinal.class);
//                startActivityForResult(intent, REQUEST_CODE);
////                replaceFragment(new Filter());
//
//            }
//        });

        //subservice
//        subServiceResponseArrayList = new ArrayList<>();
//        subServicesAdpter = new SubServicesFilterAdpter(subServiceResponseArrayList, context);
        recycler_service_sub = view.findViewById(R.id.sub_services_recycle);
        recycler_service_sub.setLayoutManager(new LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false));
        recycler_service_sub.setAdapter(subServicesAdpter);

        subServicesAdpter.notifyDataSetChanged();// Notify the adapter
//        subServicesAdpter.setFramenInterface(new SubServicesFilterAdpter.MyInterface() {
//            @Override
//            public void foo(String s) {
//                Toast.makeText(context, s, Toast.LENGTH_LONG).show();
//                //   if()
//                //
//                if (!subid.get(0).equalsIgnoreCase(s)) {
//                    subid.clear();
//                    subid.add(s);
//
//                    Toast.makeText(context, "hi i m in clear", Toast.LENGTH_LONG).show();
//
////                    replaceFragment(new parlerallDetrail(offer_id, subid));
//
//                } else {
//                    subid.clear();
//                    Toast.makeText(context, "hi i m in else", Toast.LENGTH_LONG).show();
//
//                    subid.add(s);
//
////                    replaceFragment(new parlerallDetrail(offer_id, subid));
//                }
//
//                other_product();
//            }
//        });

//        other_product();

        offerData = new ArrayList<>();
        productResults = new ArrayList<>();
        allPaulerProductDataAdpter = new AllPaulerProductDataAdpter(context, offerData, productResults);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.all_data_show);        //
        recyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(allPaulerProductDataAdpter);

//        subservice();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (getArguments() != null) {

                Bundle args = data.getBundleExtra("filterdata");
                List<String> object = (ArrayList<String>) args.getSerializable("swa");
//                Log.d("changed location=", changeaddre);
                subid=object;
//                other_product();
            }
        }
    }

//    void subservice() {
//
//        Call<SubserviceModel> call = RetrofitClient.getInstance()
//                .getapi().subservice(userid, serviceid);
//        call.enqueue(new Callback<SubserviceModel>() {
//            @Override
//            public void onResponse(Call<SubserviceModel> call, Response<SubserviceModel> response) {
//                SubserviceModel d = response.body();
//                if (d.getStatus() == true) {
//                    ArrayList<SubServiceResponse> s = new ArrayList<SubServiceResponse>(Arrays.asList(response.body().getSubserviceModel()));
//
////                Toast.makeText(CitySreen.this, "city", Toast.LENGTH_LONG).show();
//                    subServicesAdpter.updateAdapter(s, context);
//
//
//                } else {
//                    Toast.makeText(context, "" + d.getMessage(), Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<SubserviceModel> call, Throwable t) {
//
//            }
//        });
//    }
//
    void other_product() {

        Call<PaurlerAllResponse> call = RetrofitClient.getInstance()
                .getapi().palerproductandoffer(exicuteid, offer_id, subid, serviceid);
        call.enqueue(new Callback<PaurlerAllResponse>() {
            @Override
            public void onResponse(Call<PaurlerAllResponse> call, Response<PaurlerAllResponse> response) {
                PaurlerAllResponse d = response.body();

                try {

                    if (d.getStatus() == true) {
                        ArrayList<OfferData> p = new ArrayList<OfferData>(Arrays.asList(d.getData().getOfferData()));
                        ArrayList<ProductResult> g = new ArrayList<ProductResult>(Arrays.asList(response.body().getData().getProductResults()));

//                Toast.makeText(CitySreen.this, "city", Toast.LENGTH_LONG).show();
                        allPaulerProductDataAdpter.upadate(context, p, g);

                    } else {
                        Toast.makeText(context, "Product Not Available" + d.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                }

            }

            @Override
            public void onFailure(Call<PaurlerAllResponse> call, Throwable t) {

            }
        });
    }
}
