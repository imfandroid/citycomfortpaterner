package com.swapnil.citycomfortpartner.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.swapnil.citycomfortpartner.Activity.Home;
import com.swapnil.citycomfortpartner.Activity.Login;
import com.swapnil.citycomfortpartner.Adpter.AppointmentAdpter;
import com.swapnil.citycomfortpartner.Model.Request.AppointmentRequiest;
import com.swapnil.citycomfortpartner.Model.Response.AppointmentResponse;
import com.swapnil.citycomfortpartner.Model.Response.LoginResponse;
import com.swapnil.citycomfortpartner.R;
import com.swapnil.citycomfortpartner.Services.Preferences;
import com.swapnil.citycomfortpartner.Services.RetrofitClient;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Appointments extends Fragment implements TabLayout.OnTabSelectedListener {
    Context context;
    RecyclerView recycler;
    AppointmentAdpter appointmentAdpter;
    ArrayList<AppointmentResponse> appointments;
    TabLayout tabLayout;
    String exicutiveid = Preferences.getUserId();

    public Appointments() {
        // Required empty public constructor
    }

    public static Appointments newInstance() {
        Appointments fragment = new Appointments();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.appointment, container, false);
        // Inflate the layout for this fragment
        init(view);
        return view;
    }

    void init(View view) {
        context = getContext();


        appointments = new ArrayList<>();

        appointmentAdpter = new AppointmentAdpter(appointments, context);
        recycler = view.findViewById(R.id.recy_appoint);
        recycler.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        recycler.setAdapter(appointmentAdpter);
        tabLayout = view.findViewById(R.id.tabLayout);

        setupTablayout();


    }

    void setupTablayout() {

//        tabLayout.addTab(tabLayout.newTab().setText(offerResults.get(i).getOffer()));

        tabLayout.addTab(tabLayout.newTab().setText("Today's"));
        tabLayout.addTab(tabLayout.newTab().setText("Upcoming"));
        tabLayout.addTab(tabLayout.newTab().setText("Completed"));
//        replaceFragment(new TabFragment(offerResults.get(0).getOfferId()));
        tabLayout.addOnTabSelectedListener(this);
        userappointment();
    }

    void userappointment() {


        Call<AppointmentRequiest> call = RetrofitClient.getInstance()
                .getapi().appointment(exicutiveid);
        call.enqueue(new Callback<AppointmentRequiest>() {
            @Override
            public void onResponse(Call<AppointmentRequiest> call, Response<AppointmentRequiest> response) {
                AppointmentRequiest b = response.body();
                if (b.getStatus() == true) {
                    // String userid = null;

                    ArrayList<AppointmentResponse> datas = new ArrayList<AppointmentResponse>(Arrays.asList(response.body().getData()));

                    appointmentAdpter.updateAdapter(datas, context);

                } else {

                    Toast.makeText(context, "No Appointment Available", Toast.LENGTH_LONG).show();

                }


            }

            @Override
            public void onFailure(Call<AppointmentRequiest> call, Throwable t) {

            }
        });
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Log.v("tabpress", String.valueOf(tab.getPosition()));

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}