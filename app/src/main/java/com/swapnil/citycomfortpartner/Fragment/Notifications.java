package com.swapnil.citycomfortpartner.Fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.swapnil.citycomfortpartner.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Notifications extends Fragment {


    public Notifications() {
        // Required empty public constructor
    }

    public static Notifications newInstance() {
        Notifications fragment = new Notifications();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_notifications, container, false);
        // Inflate the layout for this fragment

        return view;
    }

}
