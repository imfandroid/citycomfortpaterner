package com.swapnil.citycomfortpartner.Fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.swapnil.citycomfortpartner.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Ratings extends Fragment {


    public Ratings() {
        // Required empty public constructor
    }

    public static Ratings newInstance() {
        Ratings fragment = new Ratings();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ratings, container, false);
    }

}
