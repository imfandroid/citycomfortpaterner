package com.swapnil.citycomfortpartner.Adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.swapnil.citycomfortpartner.Model.Response.SubServiceResponse;
import com.swapnil.citycomfortpartner.R;

import java.util.ArrayList;


public class SubServicesFilterAdpter extends RecyclerView.Adapter<SubServicesFilterAdpter.AllIndiaTestHolder> {
    Context context;
    ArrayList<SubServiceResponse> subServiceResponses;
    String s;
    private MyInterface myInterface;

    public SubServicesFilterAdpter(ArrayList<SubServiceResponse> subServiceResponses, Context context) {
        this.subServiceResponses = subServiceResponses;
        this.context = context;

    }


    public void updateAdapter(ArrayList<SubServiceResponse> subServiceResponses, Context context) {
        this.subServiceResponses = subServiceResponses;
        this.context = context;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AllIndiaTestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_item_subservice, parent, false);
        AllIndiaTestHolder holder = new AllIndiaTestHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull AllIndiaTestHolder holder, final int position) {
        holder.txtsubservice.setText(subServiceResponses.get(position).getSname());


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s = subServiceResponses.get(position).getSubserviceId();
                myInterface.foo(s);

            }
        });
    }

    @Override
    public int getItemCount() {
        return subServiceResponses.size();
    }

    public class AllIndiaTestHolder extends RecyclerView.ViewHolder {

        TextView txtsubservice;
        LinearLayout linearLayout;

        public AllIndiaTestHolder(View itemView) {
            super(itemView);
            txtsubservice = itemView.findViewById(R.id.txt_sub_sevice);
            linearLayout = itemView.findViewById(R.id.sublayout);


        }
    }

    public interface MyInterface {
        void foo(String s);
    }

    public void setFramenInterface(MyInterface listener) {
        this.myInterface = listener;
    }

}
