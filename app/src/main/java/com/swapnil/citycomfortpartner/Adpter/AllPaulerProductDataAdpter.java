package com.swapnil.citycomfortpartner.Adpter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import com.swapnil.citycomfortpartner.Adpter.OfferChildAdpter;

import com.swapnil.citycomfortpartner.Model.Response.CompanyArray;
import com.swapnil.citycomfortpartner.Model.Response.CompanyName;
import com.swapnil.citycomfortpartner.Model.Response.OfferData;
import com.swapnil.citycomfortpartner.Model.Response.ProductResult;
import com.swapnil.citycomfortpartner.Model.Response.Stepname;
import com.swapnil.citycomfortpartner.R;
import com.swapnil.citycomfortpartner.Services.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllPaulerProductDataAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    final int VIEW_TYPE_MESSAGE = 0;
    final int VIEW_TYPE_IMAGE = 1;

    Context context;
    List<OfferData> offerData;
    List<ProductResult> productResults;
    LinearLayoutManager linearLayout;

    OfferChildAdpter offerChildAdpter;
    ArrayList<Stepname> stepnames;
    RecyclerView recycler_view_sub, recycler_service_sub;

    List<CompanyName> companyNames = new ArrayList<>();

//    String userid = Preferences.getUserId();
    String serviceid, productid, swa;
    String producttype = "palourAtHome";

    int pos;

    public AllPaulerProductDataAdpter(Context context, List<OfferData> offerData, List<ProductResult> productResults) {
        this.context = context;
        this.offerData = offerData;
        this.productResults = productResults;
    }

    public void upadate(Context context, List<OfferData> offerData, List<ProductResult> productResults) {

        this.context = context;
        this.offerData = offerData;
        this.productResults = productResults;
       notifyDataSetChanged();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_MESSAGE) {
            View view = LayoutInflater.from(context).inflate(R.layout.single_item_offer, parent, false);
            final MessageViewHolder holder = new MessageViewHolder(view);
            init(view);
            return holder;
           // return new MessageViewHolder(LayoutInflater.from(context).inflate(R.layout.single_item_offer, parent, false));

        }

        if (viewType == VIEW_TYPE_IMAGE) {
            return new ImageViewHolder(LayoutInflater.from(context).inflate(R.layout.newlayout, parent, false));

        }

//        init(parent);
        return null;


    }


    void init(View view) {
        context = view.getContext();
        stepnames = new ArrayList<>();
        offerChildAdpter = new OfferChildAdpter(stepnames, context);

        recycler_view_sub = view.findViewById(R.id.re_step);
        linearLayout = new LinearLayoutManager(context);
        recycler_view_sub.setLayoutManager(linearLayout);
        recycler_view_sub.setLayoutManager(new LinearLayoutManager(context));
        recycler_view_sub.setAdapter(offerChildAdpter);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof MessageViewHolder) {
            if (position < offerData.size())

                ((MessageViewHolder) viewHolder).populate(offerData.get(position));
        }
        if (viewHolder instanceof ImageViewHolder) {

            if (position >= offerData.size()) {
                ((ImageViewHolder) viewHolder).populate(productResults.get(position - offerData.size()));
            }


        }


//
//        companyNames = offerData.get(position).getCompanyArray();
//        for (int j = 0; j < companyNames.size(); j++) {
//            System.out.println("companyNames()" + companyNames.get(j).getCompany() + "");
//            list.add(companyNames.get(j).getCompany());
//            priceList.add(companyNames.get(j).getPrice());
//            offersList.add(companyNames.get(j).getMyoffer());
//            discountedPrice.add(companyNames.get(j).getRate());
//        }


    }

    @Override
    public int getItemCount() {
        return offerData.size() + productResults.size();
        //

    }

    @Override
    public int getItemViewType(int position) {
        //offerData= Arrays.asList(allPaurlerResponse.getOfferData());
        //  productResults= Arrays.asList(allPaurlerResponse.getProductResults());
        Log.d("get Item view", String.valueOf(position));

        if (offerData != null && offerData.size() > position) {
            return VIEW_TYPE_MESSAGE;
        }
        //   (productResults.size() < offerData.size()-position )
        if (productResults != null) {
            Toast.makeText(context, "inside product", Toast.LENGTH_LONG).show();
            return VIEW_TYPE_IMAGE;
        }

        return -1;
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener, View.OnClickListener,
            MenuItem.OnMenuItemClickListener {
        //offer data
        TextView txttital;
        TextView txtdisp;
        TextView txtoffer;
        TextView txttotal;
        TextView txtdiscount;
        RoundedImageView imgbanner;
        //other product
        TextView txtbrand;
        LinearLayout menulayout;
        RelativeLayout layout;
//        Button detail;
        Button addtocard;

        public MessageViewHolder(View itemView) {
            super(itemView);

            txttital = itemView.findViewById(R.id.txtproductname);
            txtdisp = itemView.findViewById(R.id.txt_step_count);
            txtoffer = itemView.findViewById(R.id.txt_persent_off);
            txttotal = itemView.findViewById(R.id.txt_prise);
            txtdiscount = itemView.findViewById(R.id.txt_discount);
            imgbanner = itemView.findViewById(R.id.inm);
//            detail = itemView.findViewById(R.id.show_detail);

            layout = itemView.findViewById(R.id.layout_deatai);
            txtbrand = itemView.findViewById(R.id.txt_brand);


            addtocard = itemView.findViewById(R.id.add_to_card);
//
            addtocard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    productid = swa;
//                    addcard();
                }
            });
            menulayout = itemView.findViewById(R.id.capanyname);

            menulayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos = getAdapterPosition();
                    int p = getLayoutPosition();
                    int e = getOldPosition();

                    System.out.println("getAdapterPosition()=" + getAdapterPosition() + "");
                    System.out.println("getLayoutPosition()=" + getLayoutPosition() + "");
                    // System.out.println("getOldPosition()" + getOldPosition() + "");
                    companyNames = offerData.get(pos).getCompanyArray();
                    for (int j = 0; j < companyNames.size(); j++) {
                        System.out.println("companyNames()" + companyNames.get(j).getCompanyName() + "");
                    }
                    // notifyItemChanged(getAdapterPosition());
                    //  itemView.getParent().showContextMenuForChild(itemView);
                    v.getParent().showContextMenuForChild(v);
                }
            });
            menulayout.setOnCreateContextMenuListener(this);


        }

        public void populate(OfferData offerData) {

            swa = offerData.getProductId();
            txttital.setText(offerData.getPname());
            txtdisp.setText(offerData.getServiceStepName());
            txtoffer.setText(offerData.getCompanyArray().get(0).getMyoffer());
            txttotal.setText("\u20B9 " + offerData.getCompanyArray().get(0).getPrice());
            txtdiscount.setText("\u20B9 " + offerData.getCompanyArray().get(0).getRate());
            Picasso.with(context).load(offerData.getImagepath()).into(imgbanner);
            //stepnames = offerData.get(position).getStepname();
            txtdiscount.setPaintFlags(txtdiscount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//
//            detail.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String productid = offerData.getProductId();
//                    Intent intent = new Intent(context, ParlorStepDetail.class);
//                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    intent.putExtra("productid", productid);
//                    context.startActivity(intent);
//
//                }
//            });

//            for(int i=0;i< offerData.getStepname().size();i++){
//
//                offerChildAdpter.updateAdapter(offerData.getStepname(), context);
//
//
//            }
//            layout.setOnClickListener(new View.OnClickListener() {
//                @SuppressLint("ResourceType")
//                @Override
//                public void onClick(View v) {
//                    String productid = offerData.getProductId();
//                    AllOfferOtherData myActivity = (AllOfferOtherData) context;
//
//                    PalerStepDetail fragment2 = new PalerStepDetail();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("productid", productid);
//                    fragment2.setArguments(bundle);
//                    FragmentManager fragmentManager = myActivity.getSupportFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.news, fragment2);
//                    //  fragmentTransaction.addToBackStack("swa");
//                    fragmentTransaction.commit();
//                }
//            });
            //  offerChildAdpter.updateAdapter(stepnames, context);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {

            System.out.println("item.getGroupId()" + item.getGroupId() + "");


            List<CompanyName> ls = offerData.get(getAdapterPosition()).getCompanyArray();

            System.out.println("size of ls=" + ls.size() + "");
            for (int i = 0; i < ls.size(); i++) {
                if (ls.get(i).getCompanyName().equalsIgnoreCase(String.valueOf(item))) {
                    txtdiscount.setText("");
                    txttotal.setText("");
                    txtoffer.setText("");

//                    ls.get(i).getId()
                    txttotal.setText("\u20B9 " + ls.get(i).getPrice());
                    txtdiscount.setText("\u20B9 " + ls.get(i).getRate());
                    txtbrand.setText(ls.get(i).getCompanyName());

                    txtdiscount.setPaintFlags(txtdiscount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


                }

            }
            return false;
        }

        @Override
        public void onClick(View view) {

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Choose a filter");
            MenuItem myActionItem;
            for (int j = 0; j < companyNames.size(); j++) {
                myActionItem = menu.add(this.getAdapterPosition(), v.getId(), j, companyNames.get(j).getCompanyName());
                // myActionItem = menu.add(companyNames.get(j).getCompany());

                myActionItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {


                        List<CompanyName> ls = offerData.get(getAdapterPosition()).getCompanyArray();

//                        System.out.println("item.isChecked()=" + item.isChecked() == true + "");
                        for (int i = 0; i < ls.size(); i++) {
//                            System.out.println("value of item swapnil " + item.getItemId());
//                            System.out.println("value of company name pranjali " + ls.get(i).getCompany());
//                            System.out.println("value of String.valueOf(item) " + String.valueOf(item));
                            if (ls.get(i).getCompanyName().equalsIgnoreCase(String.valueOf(item))) {

//                                System.out.println("index of selected item " + ls.get(i).getCompany());
//                                System.out.println("index of selected item " + ls.get(i).getId() + "");
//                                System.out.println("price of item=" + ls.get(i).getPrice());
//                                System.out.println("getRate of item=" + ls.get(i).getRate());
//                                System.out.println("getMyoffer of item=" + ls.get(i).getMyoffer());
                                txtdiscount.setText("");
                                txttotal.setText("");
                                txtoffer.setText("");
                                txttotal.setText("\u20B9 " + ls.get(i).getPrice());
                                txtdiscount.setText("\u20B9 " + ls.get(i).getRate());
                                txtbrand.setText(ls.get(i).getCompanyName());
                                txtoffer.setText(ls.get(i).getMyoffer());
//                                System.out.println("textview value" + txtoffer.getText());
//                                System.out.println("textview value" + txttotal.getText());
//                                System.out.println("textview value" + txtdiscount.getText());
                                txtdiscount.setPaintFlags(txtdiscount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

//                    ActionBar actionBar = get();
                                break;
                            }
                            // mISQuizItemSelected = true;
                        }

                        return true;
                    }
                });
                //     System.out.println("position of item="+list.get(j)+"");
            }
        }
    }

//    void addcard() {
//
//        Call<ResponseBody> call = RetrofitClient.getInstance()
//                .getapi().addtocart(userid, productid, producttype);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                ResponseBody d = response.body();
//                //  ArrayList<SubServiceResponse> s = new ArrayList<SubServiceResponse>(Arrays.asList(response.body().getSubserviceModel()));
//                if (d != null) {
//
//                    Toast.makeText(context, "Product Added Successfully", Toast.LENGTH_LONG).show();
//
//
//                } else {
//                    Toast.makeText(context, "Product NOT Added Successfully", Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//            }
//        });
//    }

    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener,
            MenuItem.OnMenuItemClickListener {

        TextView txttital;
        TextView txtdisp;
        TextView txtoffer;
        TextView txttotal;
        TextView txtdiscount;
        RoundedImageView imgbanner;
        LinearLayout layout;
        LinearLayout menulayout;
        Button detail;
        List<CompanyArray> companyNames = new ArrayList<>();
        int pos;
        Button addtocard;
        //Spinner vspinner;

        TextView txtbrand;

        public ImageViewHolder(View itemView) {
            super(itemView);

            txttital = itemView.findViewById(R.id.txtproductname);
            txttotal = itemView.findViewById(R.id.txt_prise);
            imgbanner = itemView.findViewById(R.id.inm);
//            layout = itemView.findViewById(R.id.layout_dea);
//            detail = itemView.findViewById(R.id.show_detail);
            txtbrand = itemView.findViewById(R.id.txt_brand);
            menulayout = itemView.findViewById(R.id.capanyname1);
            menulayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    System.out.println("getAdapterPosition()=" + pos + "");
//        System.out.println("getLayoutPosition()=" + getLayoutPosition() + "");
                    // System.out.println("getOldPosition()" + getOldPosition() + "");
                    companyNames = productResults.get(pos).getCompanyArray();
                    for (int j = 0; j < companyNames.size(); j++) {
                        System.out.println("companyNames()" + companyNames.get(j).getCompany() + "");
                    }

                    v.getParent().showContextMenuForChild(v);

                }
            });
            menulayout.setOnCreateContextMenuListener(this);

            addtocard = itemView.findViewById(R.id.add_to_card);
//
            addtocard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    productid = swa;
//                    addcard();
                }
            });

        }

        public void populate(ProductResult productResults) {
            swa = productResults.getProductId();
            txttital.setText(productResults.getPname());
            txttotal.setText(productResults.getCompanyArray().get(0).getRate());
            Picasso.with(context).load(productResults.getImagepath()).into(imgbanner);
            //   stepnames=productResults.get(position).getStepname();

//
//            detail.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String productid = productResults.getProductId();
//                    Intent intent = new Intent(context, ParlorStepDetail.class);
//                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    intent.putExtra("productid", productid);
//                    context.startActivity(intent);
//                }
//            });
//            layout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            return false;
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Choose a filter");
            MenuItem myActionItem;
            for (int j = 0; j < companyNames.size(); j++) {
                myActionItem = menu.add(this.pos, v.getId(), j, companyNames.get(j).getCompanyName());
                // myActionItem = menu.add(companyNames.get(j).getCompany());

                myActionItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {


                        List<CompanyArray> ls = productResults.get(pos).getCompanyArray();

                        System.out.println("item.isChecked()=" + item.isChecked() == true + "");
                        for (int i = 0; i < ls.size(); i++) {

                            if (ls.get(i).getCompanyName().equalsIgnoreCase(String.valueOf(item))) {
                                System.out.println("price of item=" + ls.get(i).getRate());
                                txttotal.setText("");
                                txttotal.setText("\u20B9 " + ls.get(i).getRate());
                                txtbrand.setText(ls.get(i).getCompanyName());
                                break;
                            }

                        }

                        return true;
                    }
                });

            }
        }
    }
//    void filtercall(){
//
//        FragmentManager fm = context.;
////fragment class name : DFragment
//        DFragment dFragment = new DFragment();
//        // Show DialogFragment
//        dFragment.show(fm, "Dialog Fragment");
//    }

}
