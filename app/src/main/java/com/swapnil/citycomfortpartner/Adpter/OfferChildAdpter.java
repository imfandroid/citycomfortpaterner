package com.swapnil.citycomfortpartner.Adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.swapnil.citycomfortpartner.Model.Response.Stepname;
import com.swapnil.citycomfortpartner.R;

import java.util.ArrayList;


public class OfferChildAdpter extends RecyclerView.Adapter<OfferChildAdpter.AllIndiaTestHolder> {
    Context context;
    ArrayList<Stepname> stepnames;

//    RecyclerView recycler_view_sub;
//    OfferSingleAdpter offerSingleAdpter;

    public OfferChildAdpter(ArrayList<Stepname> stepnames, Context context) {
        this.stepnames = stepnames;
        this.context = context;
    }


    public void updateAdapter(ArrayList<Stepname> stepnames, Context context) {
        this.stepnames = stepnames;
        this.context = context;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AllIndiaTestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_child_cycle, parent, false);
        AllIndiaTestHolder holder = new AllIndiaTestHolder(view);
//        init(view);
        return holder;
    }
//    void  init(View view){
//        context = view.getContext();
//        offerData=new ArrayList<>();
//        recycler_view_sub=view.findViewById(R.id.txt_step);
//        offerSingleAdpter = new OfferSingleAdpter(offerData, context);
//       // recycler_view_sub.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true));
//        recycler_view_sub.setAdapter(offerSingleAdpter);
//
//
//    }

    @Override
    public void onBindViewHolder(@NonNull AllIndiaTestHolder holder, int position) {

        int c = position + 1;
        holder.txtsubservice.setText("Step " + c + stepnames.get(position).getStepname());
        // Log.d("stepnames.get(position).getStepname()",stepnames.get(position).getStepname());
//        Picasso.with(context).load(subServiceResponses.get(position).getLogo()).into(holder.imgLogo);
    }

    @Override
    public int getItemCount() {
        return stepnames.size();
    }

    public class AllIndiaTestHolder extends RecyclerView.ViewHolder {

        TextView txtsubservice;

        public AllIndiaTestHolder(View itemView) {
            super(itemView);
            txtsubservice = itemView.findViewById(R.id.txt_step);
        }
    }
//    void subservice(){
//        Call<OfferResponse> call= RetrofitClient.getInstance()
//                .getapi().offer_single();
//        call.enqueue(new Callback<OfferResponse>() {
//            @Override
//            public void onResponse(Call<OfferResponse> call, Response<OfferResponse> response) {
//                OfferResponse d = response.body();
//                //   ArrayList<OfferData> s = new ArrayList<OfferData>(ArrayList);
//                ArrayList<OfferResult> s = new ArrayList<OfferResult>(Arrays.asList(response.body().getOfferResult()));
//
//                if (d.isStatus() == true) {
//
////                Toast.makeText(CitySreen.this, "city", Toast.LENGTH_LONG).show();
//                    //   offerSingleAdpter.updateAdapter(s,context);
//
//                }else {
//                    Toast.makeText(context, ""+d.getMessage(), Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<OfferResponse> call, Throwable t) {
//
//            }
//        });
//    }
}
