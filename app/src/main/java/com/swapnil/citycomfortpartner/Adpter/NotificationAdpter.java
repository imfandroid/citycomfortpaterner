package com.swapnil.citycomfortpartner.Adpter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.swapnil.citycomfortpartner.Activity.AppointmentDetail;
import com.swapnil.citycomfortpartner.Model.Response.AppointmentResponse;
import com.swapnil.citycomfortpartner.R;

import java.util.ArrayList;


public class NotificationAdpter extends RecyclerView.Adapter<NotificationAdpter.AllIndiaTestHolder> {
    Context context;
    ArrayList<AppointmentResponse> appointmentResponses;
    private LayoutInflater inflater;

    public NotificationAdpter(ArrayList<AppointmentResponse> appointmentResponses, Context context) {
        this.appointmentResponses = appointmentResponses;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    public void updateAdapter( ArrayList<AppointmentResponse> appointmentResponses, Context context) {
        this.appointmentResponses = appointmentResponses;
        this.context = context;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AllIndiaTestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_notification, parent, false);
        AllIndiaTestHolder holder = new AllIndiaTestHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull AllIndiaTestHolder holder, final int position) {


        holder.txtclintname.setText(appointmentResponses.get(position).getFullNameeeee());

        holder.courcelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String productid = appointmentResponses.get(position).getProductId();
                Intent intent = new Intent(context, AppointmentDetail.class);
                intent.putExtra("productid", productid);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return appointmentResponses.size();
    }

    public class AllIndiaTestHolder extends RecyclerView.ViewHolder {

        TextView txtclintname;
        ImageView callimg;
        LinearLayout courcelayout;


        public AllIndiaTestHolder(View itemView) {
            super(itemView);
            txtclintname = itemView.findViewById(R.id.clint_name);
            callimg = itemView.findViewById(R.id.call_img);

            courcelayout = itemView.findViewById(R.id.appointment_detail);
        }
    }

}
