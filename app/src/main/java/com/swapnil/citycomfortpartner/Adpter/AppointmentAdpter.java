package com.swapnil.citycomfortpartner.Adpter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.swapnil.citycomfortpartner.Activity.AppointmentDetail;
import com.swapnil.citycomfortpartner.Model.Response.AppointmentResponse;
import com.swapnil.citycomfortpartner.R;

import java.util.ArrayList;


public class AppointmentAdpter extends RecyclerView.Adapter<AppointmentAdpter.AllIndiaTestHolder> {
    Context context;
    ArrayList<AppointmentResponse> appointmentResponses;
    private LayoutInflater inflater;

    public AppointmentAdpter( ArrayList<AppointmentResponse> appointmentResponses, Context context) {
        this.appointmentResponses = appointmentResponses;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    public void updateAdapter( ArrayList<AppointmentResponse> appointmentResponses, Context context) {
        this.appointmentResponses = appointmentResponses;
        this.context = context;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AllIndiaTestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_item_appointment, parent, false);
        AllIndiaTestHolder holder = new AllIndiaTestHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull AllIndiaTestHolder holder, final int position) {


        holder.txtclintname.setText(appointmentResponses.get(position).getFullNameeeee());
        holder.txttime.setText(appointmentResponses.get(position).getParlourTime());
        holder.txtservicename.setText(appointmentResponses.get(position).getPname());
        holder.txtmobilenumber.setText(appointmentResponses.get(position).getMobileNumber());
        holder.txtclintaddress.setText(appointmentResponses.get(position).getAddress());
        holder.txtcompanyname.setText(appointmentResponses.get(position).getCompanyyy());

        holder.courcelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String productid = appointmentResponses.get(position).getProductId();
                Intent intent = new Intent(context, AppointmentDetail.class);
                intent.putExtra("productid", productid);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return appointmentResponses.size();
    }

    public class AllIndiaTestHolder extends RecyclerView.ViewHolder {

        TextView txtclintname;
        TextView txttime;
        TextView txtservicename;
        TextView txtmobilenumber;
        TextView txtclintaddress;
        TextView txtcompanyname;
        ImageView callimg;
        LinearLayout courcelayout;


        public AllIndiaTestHolder(View itemView) {
            super(itemView);
            txtclintname = itemView.findViewById(R.id.clint_name);
            txttime = itemView.findViewById(R.id.appoint_time);
            txtservicename = itemView.findViewById(R.id.service_name);
            txtmobilenumber = itemView.findViewById(R.id.clint_number);
            txtclintaddress = itemView.findViewById(R.id.clint_address);
            txtcompanyname=itemView.findViewById(R.id.company_name);
            callimg = itemView.findViewById(R.id.call_img);

            courcelayout = itemView.findViewById(R.id.appointment_detail);
        }
    }

}
