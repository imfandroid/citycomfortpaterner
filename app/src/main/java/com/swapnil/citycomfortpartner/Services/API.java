package com.swapnil.citycomfortpartner.Services;




import com.swapnil.citycomfortpartner.Model.Request.AppintmentDetailRequiest;
import com.swapnil.citycomfortpartner.Model.Request.AppointmentRequiest;
import com.swapnil.citycomfortpartner.Model.Request.PaurlerAllResponse;
import com.swapnil.citycomfortpartner.Model.Response.LoginResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface API {


    @FormUrlEncoded
    @POST("executive_login")
    Call<LoginResponse> login(

            @Field("ext_mobile") String mobile_no,
            @Field("ext_password") String password


    );

    @FormUrlEncoded
    @POST("executive_appointments")
    Call<AppointmentRequiest> appointment(
            @Field("ext_id") String exicutive_id
//            @Field("ext_password") String password


    );

    @FormUrlEncoded
    @POST("appointments_details")
    Call<AppintmentDetailRequiest> appointment_detail(
            @Field("product_id") String product_id
//            @Field("ext_password") String password

    );
    @FormUrlEncoded
    @POST("fetch_filter_subservice")
    Call<PaurlerAllResponse> palerproductandoffer(
            @Field("id") String userid,
            @Field("offer_id") String offerid,
            @Field("subservice_id[]") List<String> subserviceid,
            @Field("service_id") String serviceid
    );

}
