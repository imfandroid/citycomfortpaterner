package com.swapnil.citycomfortpartner.Services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static final String BASE_URL = "http://192.168.29.102:1414";

//     private static final String BASE_URL="http://52.66.31.111:5000";


    private static RetrofitClient mInstance;
    private Retrofit retrofit;
    private RetrofitClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();

        }
        return mInstance;
    }

    public API getapi() {
        return retrofit.create(API.class);
    }
}
