package com.swapnil.citycomfortpartner.Services;

/**
 * Created by JASS-3 on 5/19/2018.
 */

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;


public class Preferences {

    private final static String preferencesName = "CityComfort";//
    public static Context appContext;


    public static void setUserId(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("setUserId", val);
        editor.commit();
    }
    public static String getUserId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("setUserId", null);
        return value;
    }

    public static void setLoginStatus(boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("loginStatus",value);
        //editor.putString("getCurrentUserId", value);
        editor.commit();
    }
    public static boolean getLoginStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("loginStatus", false);
        return value;
    }




    public static void setSubjectId(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("subject_id", val);
        editor.commit();
    }
    public static String getSubjectId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("subject_id", null);
        return value;
    }

    public static void setUsername(String val)
    {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("Username", val);
        editor.commit();
    }
    public static String getUsername() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("Username", null);
        return value;
    }

}

