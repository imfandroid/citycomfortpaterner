package com.swapnil.citycomfortpartner.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductResult {

    @SerializedName("service_step_name")
    @Expose
    private String serviceStepName;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("pname")
    @Expose
    private String pname;
    @SerializedName("imagepath")
    @Expose

    private String imagepath;
    @SerializedName("stepname")
    @Expose
    private List< StepnameOtherProduct> stepname = null;
    @SerializedName("company_array")
    @Expose
    private List<CompanyArray> companyArray = null;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public List<StepnameOtherProduct> getStepname() {
        return stepname;
    }

    public void setStepname(List<StepnameOtherProduct> stepname) {
        this.stepname = stepname;
    }

    public List<CompanyArray> getCompanyArray() {
        return companyArray;
    }

    public void setCompanyArray(List<CompanyArray> companyArray) {
        this.companyArray = companyArray;
    }

    public String getServiceStepName() {
        return serviceStepName;
    }

    public void setServiceStepName(String serviceStepName) {
        this.serviceStepName = serviceStepName;
    }
}
