package com.swapnil.citycomfortpartner.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyName {
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("myoffer")
    @Expose
    private String myoffer;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMyoffer() {
        return myoffer;
    }

    public void setMyoffer(String myoffer) {
        this.myoffer = myoffer;
    }
}
