package com.swapnil.citycomfortpartner.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppintmentDetailResponse {
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("parlour_date")
    @Expose
    private String parlourDate;
    @SerializedName("parlour_time")
    @Expose
    private String parlourTime;
    @SerializedName("full_nameeeee")
    @Expose
    private String fullNameeeee;
    @SerializedName("pname")
    @Expose
    private String pname;
    @SerializedName("service_step_name")
    @Expose
    private String serviceStepName;
    @SerializedName("stepname")
    @Expose
    private List<StepNameResponse> stepname = null;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getParlourDate() {
        return parlourDate;
    }

    public void setParlourDate(String parlourDate) {
        this.parlourDate = parlourDate;
    }

    public String getParlourTime() {
        return parlourTime;
    }

    public void setParlourTime(String parlourTime) {
        this.parlourTime = parlourTime;
    }

    public String getFullNameeeee() {
        return fullNameeeee;
    }

    public void setFullNameeeee(String fullNameeeee) {
        this.fullNameeeee = fullNameeeee;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getServiceStepName() {
        return serviceStepName;
    }

    public void setServiceStepName(String serviceStepName) {
        this.serviceStepName = serviceStepName;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public List<StepNameResponse> getStepname() {
        return stepname;
    }

    public void setStepname(List<StepNameResponse> stepname) {
        this.stepname = stepname;
    }
}
