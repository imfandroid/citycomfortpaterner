package com.swapnil.citycomfortpartner.Model.Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.swapnil.citycomfortpartner.Model.Response.OfferData;
import com.swapnil.citycomfortpartner.Model.Response.ProductResult;

public class AllPaurlerResponse {
    @SerializedName("offers_product")
    @Expose
    private OfferData[] offerData = null;
    @SerializedName("other_product")
    @Expose
    private ProductResult[] productResults = null;

    public OfferData[] getOfferData() {
        return offerData;
    }

    public void setOfferData(OfferData[] offerData) {
        this.offerData = offerData;
    }

    public ProductResult[] getProductResults() {
        return productResults;
    }

    public void setProductResults(ProductResult[] productResults) {
        this.productResults = productResults;
    }
}
