package com.swapnil.citycomfortpartner.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stepname {

    @SerializedName("stepname")
    @Expose
    private String stepname;
    @SerializedName("imagepath")
    @Expose
    private String imagepath;

    public String getStepname() {
        return stepname;
    }

    public void setStepname(String stepname) {
        this.stepname = stepname;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

}

