package com.swapnil.citycomfortpartner.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OfferData {


    @SerializedName("cart_id")
    @Expose
    private String cartId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("pname")
    @Expose
    private String pname;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("imagepath")
    @Expose
    private String imagepath;
    @SerializedName("service_step_name")
    @Expose
    private String serviceStepName;
    @SerializedName("stepname")
    @Expose
    private List<Stepname> stepname = null;
    @SerializedName("company_array")
    @Expose
    private List<CompanyName> companyArray = null;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }



    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getServiceStepName() {
        return serviceStepName;
    }

    public void setServiceStepName(String serviceStepName) {
        this.serviceStepName = serviceStepName;
    }

    public List<Stepname> getStepname() {
        return stepname;
    }

    public void setStepname(List<Stepname> stepname) {
        this.stepname = stepname;
    }

    public List<CompanyName> getCompanyArray() {
        return companyArray;
    }

    public void setCompanyArray(List<CompanyName> companyArray) {
        this.companyArray = companyArray;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }
}