package com.swapnil.citycomfortpartner.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppointmentResponse {
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("pname")
    @Expose
    private String pname;
    @SerializedName("companyyy")
    @Expose
    private String companyyy;
    @SerializedName("full_nameeeee")
    @Expose
    private String fullNameeeee;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("parlour_date")
    @Expose
    private String parlourDate;
    @SerializedName("parlour_time")
    @Expose
    private String parlourTime;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getCompanyyy() {
        return companyyy;
    }

    public void setCompanyyy(String companyyy) {
        this.companyyy = companyyy;
    }

    public String getFullNameeeee() {
        return fullNameeeee;
    }

    public void setFullNameeeee(String fullNameeeee) {
        this.fullNameeeee = fullNameeeee;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getParlourDate() {
        return parlourDate;
    }

    public void setParlourDate(String parlourDate) {
        this.parlourDate = parlourDate;
    }

    public String getParlourTime() {
        return parlourTime;
    }

    public void setParlourTime(String parlourTime) {
        this.parlourTime = parlourTime;
    }
}
