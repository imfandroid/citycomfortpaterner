package com.swapnil.citycomfortpartner.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StepNameResponse {
    @SerializedName("stepname")
    @Expose
    private String stepname;

    public String getStepname() {
        return stepname;
    }

    public void setStepname(String stepname) {
        this.stepname = stepname;
    }
}
