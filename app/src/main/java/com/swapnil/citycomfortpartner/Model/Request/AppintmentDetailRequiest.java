package com.swapnil.citycomfortpartner.Model.Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.swapnil.citycomfortpartner.Activity.AppointmentDetail;
import com.swapnil.citycomfortpartner.Model.Response.AppintmentDetailResponse;
import com.swapnil.citycomfortpartner.Model.Response.AppointmentResponse;

public class AppintmentDetailRequiest {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private AppintmentDetailResponse[] appintmentDetailResponses = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AppintmentDetailResponse[] getAppintmentDetailResponses() {
        return appintmentDetailResponses;
    }

    public void setAppintmentDetailResponses(AppintmentDetailResponse[] appintmentDetailResponses) {
        this.appintmentDetailResponses = appintmentDetailResponses;
    }
}
