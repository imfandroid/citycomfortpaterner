package com.swapnil.citycomfortpartner.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.swapnil.citycomfortpartner.Fragment.FinalOfferFilter;
import com.swapnil.citycomfortpartner.Model.Request.AppintmentDetailRequiest;
import com.swapnil.citycomfortpartner.Model.Request.AppointmentRequiest;
import com.swapnil.citycomfortpartner.Model.Response.AppintmentDetailResponse;
import com.swapnil.citycomfortpartner.Model.Response.AppointmentResponse;
import com.swapnil.citycomfortpartner.Model.Response.LoginResponse;
import com.swapnil.citycomfortpartner.R;
import com.swapnil.citycomfortpartner.Services.Preferences;
import com.swapnil.citycomfortpartner.Services.RetrofitClient;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppointmentDetail extends AppCompatActivity {
    Context context;
    TextView txtname, txtmobile, txtaddress, txttime, txtdate, txtproductname, txtsteptital, txtstepdetail, txtprise;
    Button btnaddservice, btnstarttime;
    ImageView imgcall;
    String Productid,serviceid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_detail);
        init();
    }

    void init() {
        context = AppointmentDetail.this;
        txtname = findViewById(R.id.clint_name);
        txtmobile = findViewById(R.id.clint_num);
        txtaddress = findViewById(R.id.clint_address);
        txtdate = findViewById(R.id.clint_date);
        txtproductname = findViewById(R.id.product_name);
        txtsteptital = findViewById(R.id.product_step_name);
        txtstepdetail = findViewById(R.id.product_detail_step);
        txtprise = findViewById(R.id.product_prise);
        txttime = findViewById(R.id.clint_time);
        btnaddservice = findViewById(R.id.btn_add_sevice);
        btnstarttime = findViewById(R.id.btn_start_timer);
        imgcall = findViewById(R.id.img_call);
        Intent intent = getIntent();
        Productid = intent.getStringExtra("productid");

        fetchdetail();
        btnaddservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                AllOfferOtherData myActivity = (AllOfferOtherData) context;
                FinalOfferFilter fragment2 = new FinalOfferFilter();
                Bundle bundle = new Bundle();
                bundle.putString("serviceid", serviceid);
                fragment2.setArguments(bundle);
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.main_fram, fragment2);
                fragmentTransaction.commit();


            }
        });
        btnstarttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imgcall.setOnClickListener(new View.OnClickListener() {
            Intent call = new Intent(Intent.ACTION_DIAL);

            @Override
            public void onClick(View view) {

//                call.setData(Uri.parse("Telnr:" + txtmobile.getText()));
//                startActivity(call);

                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", txtmobile.getText().toString(), null));
                startActivity(intent);
            }
        });

    }

    void fetchdetail() {


        Call<AppintmentDetailRequiest> call = RetrofitClient.getInstance()
                .getapi().appointment_detail(Productid);
        call.enqueue(new Callback<AppintmentDetailRequiest>() {
            @Override
            public void onResponse(Call<AppintmentDetailRequiest> call, Response<AppintmentDetailRequiest> response) {
                AppintmentDetailRequiest b = response.body();
                if (b.getStatus() == true) {
                    // String userid = null;

                    ArrayList<AppintmentDetailResponse> datas = new ArrayList<AppintmentDetailResponse>(Arrays.asList(response.body().getAppintmentDetailResponses()));
                    serviceid=datas.get(0).getServiceId();
                    txtname.setText(datas.get(0).getFullNameeeee());
                    txtmobile.setText(datas.get(0).getMobileNumber());
                    txtaddress.setText(datas.get(0).getAddress());
                    txtdate.setText(datas.get(0).getParlourDate());
                    txtproductname.setText(datas.get(0).getPname());
                    txtsteptital.setText(datas.get(0).getServiceStepName());
                    txtstepdetail.setText("");
                    txtprise.setText(datas.get(0).getParlourTime());
                    txttime.setText(datas.get(0).getParlourTime());

//                    appointmentAdpter.updateAdapter(datas, context);
                    String s1 = "";
                    for (int j = 0; j < datas.get(0).getStepname().size(); j++) {

                        String c = datas.get(0).getStepname().get(j).getStepname()
                                .concat("\n");
                        s1 = c.concat(s1).concat("\n");
                        txtstepdetail.setText(s1);

                    }

                } else {

                    Toast.makeText(context, "No Detail Available", Toast.LENGTH_LONG).show();

                }


            }

            @Override
            public void onFailure(Call<AppintmentDetailRequiest> call, Throwable t) {

            }
        });
    }

}
