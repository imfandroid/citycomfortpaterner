package com.swapnil.citycomfortpartner.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.swapnil.citycomfortpartner.Model.Response.LoginResponse;
import com.swapnil.citycomfortpartner.R;
import com.swapnil.citycomfortpartner.Services.Preferences;
import com.swapnil.citycomfortpartner.Services.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    TextInputEditText txtusername, txtpassword;
    String username, password;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        init();

    }

    void init() {

        Preferences.appContext = getApplicationContext();

        txtusername = findViewById(R.id.loginmob);
        txtpassword = findViewById(R.id.loginpassword);
        login = findViewById(R.id.btn_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginuser();
            }
        });
    }

    void getdata() {

        username = txtusername.getText().toString();
        password = txtpassword.getText().toString();

    }

    void loginuser() {
        getdata();

        Call<LoginResponse> call = RetrofitClient.getInstance()
                .getapi().login(username, password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse b = response.body();
                if (b.getStatus() == true) {
                    // String userid = null;

                    Preferences.setLoginStatus(true);
                    Preferences.setUserId(b.getExecutiveId());
                    Intent intent = new Intent(Login.this, Home.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivity(intent);


                } else {

                    Intent intent = new Intent(Login.this, Login.class);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    //  intent.putExtra("userid",b.getId());
                    startActivity(intent);
                    Toast.makeText(Login.this, "Invalid Mobile Number And Password", Toast.LENGTH_LONG).show();

                }


            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });
    }
}
