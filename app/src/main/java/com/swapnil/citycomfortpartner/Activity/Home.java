package com.swapnil.citycomfortpartner.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.swapnil.citycomfortpartner.Fragment.Appointments;
import com.swapnil.citycomfortpartner.Fragment.Notifications;
import com.swapnil.citycomfortpartner.Fragment.Payment;
import com.swapnil.citycomfortpartner.Fragment.Product;
import com.swapnil.citycomfortpartner.Fragment.Ratings;
import com.swapnil.citycomfortpartner.R;

public class Home extends AppCompatActivity {
    Context context;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        init();
    }

    void init() {
        context = Home.this;
        bottomNavigationView = findViewById(R.id.navigation);
        onClick();
    }

    void onClick() {

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                Fragment selectedFragment = null;
                switch (menuItem.getItemId()) {


                    case R.id.home:
                        selectedFragment = Appointments.newInstance();
                        break;
                    case R.id.my_booking:
                        selectedFragment = Payment.newInstance();
                        break;
                    case R.id.cart:
//                        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
//                            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//                        }
                        selectedFragment = Ratings.newInstance();
                        break;

                    case R.id.cartc:
//                        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
//                            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//                        }
                        selectedFragment = Notifications.newInstance();
                        break;
                    case R.id.carte:
//                        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
//                            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//                        }
                        selectedFragment = Product.newInstance();
                        break;
//                    case R.id.profil:
//                        selectedFragment = Profile.newInstance();
//                        break;
//                    case R.id.wallet:
//                        selectedFragment = Wallet.newInstance();
//                        //     selectedFragment = ItemThreeFragment.newInstance();
//                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                assert selectedFragment != null;
                transaction.replace(R.id.main_fram, selectedFragment);
                transaction.commit();
                return true;
            }
        });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_fram, Appointments.newInstance());

        transaction.commit();
    }
}
